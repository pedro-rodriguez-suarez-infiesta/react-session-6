import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";


function Header() {
  return (
    <header>
      <span>
        <Link to="/home">
          <FontAwesomeIcon icon={faHome} className="icon__header" />
        </Link>
      </span>
    </header>
  );
}
export default Header