import { Switch, Route } from 'react-router-dom';

import { useState, useEffect } from "react";
import "./App.css";

import Home from '../src/pages/home';
import CompletedTasks from './pages/completed-tasks'


import Header from "../src/components/header";

function App() {
  const toDo = {
    todos: [],
    completed: [],
  };
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/" exact>
         <Home />
        </Route>

        <Route path="/completed-tasks" exact>
          <CompletedTasks/>
        </Route>

        </Switch>


    </div>
  );
}

export default App;
